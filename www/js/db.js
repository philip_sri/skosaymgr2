angular.module('skosayMgr.db', [])

.controller("login.Db.Controller", function($scope, $cordovaSQLite) {
 
    $scope.insert = function(email, password, access_token) {
        
        if ($scope.find(email) == 0){
            
        var query = "INSERT INTO login (email, password, access_token) VALUES (?,?,?)";
        $cordovaSQLite.execute(db, query, [email, password, access_token]).then(function(res) {
            console.log("INSERT ID -> " + res.insertId);
        }, function (err) {
            console.error(err);
        })
        
        }
        else {
            var query = "UPDATE login SET (email = 'email', password = 'password', access_token = 'access_token') VALUES (?,?,?)";
            $cordovaSQLite.execute(db, query, [firstname, lastname])
                .then(function(res) {
                    console.log("UPDATE ID -> " + res.insertId);
                                    }, 
                function (err) {
                    console.error(err);
                                }
                );
        }
    }
    
    $scope.select = function(lastname) {
        var query = "SELECT firstname, lastname FROM people WHERE lastname = ?";
        $cordovaSQLite.execute(db, query, [lastname]).then(function(res) {
            if(res.rows.length > 0) {
                console.log("SELECTED -> " + res.rows.item(0).firstname + " " + res.rows.item(0).lastname);
            } else {
                console.log("No results found");
            }
        }, function (err) {
            console.error(err);
        });
    }

    $scope.find = function(email) {
        var query = "SELECT email FROM login WHERE email = ?";
        $cordovaSQLite.execute(db, query, [email]).then(function(res) {
            if(res.rows.length > 0) {
                console.log("SELECTED -> " + res.rows.item(0).email);
            } else {
                console.log("No results found");
            }
        }, function (err) {
            console.error(err);
        });
    }
    
    $scope.insertMessage = function(firstname, lastname) {
        var query = "INSERT INTO messages (firstname, lastname) VALUES (?,?)";
        $cordovaSQLite.execute(db, query, [firstname, lastname]).then(function(res) {
            console.log("INSERT ID -> " + res.insertId);
        }, function (err) {
            console.error(err);
        });
    }
 
    $scope.insertIssue = function(firstname, lastname) {
        var query = "INSERT INTO issues (firstname, lastname) VALUES (?,?)";
        $cordovaSQLite.execute(db, query, [firstname, lastname]).then(function(res) {
            console.log("INSERT ID -> " + res.insertId);
        }, function (err) {
            console.error(err);
        });
    }
 
});