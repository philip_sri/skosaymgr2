angular.module('skosayMgr.issues', [])

.controller('NewIssuesController', function($rootScope, $http, $ionicPopup, $scope, $state, $stateParams, $ionicScrollDelegate, $localstorage, $ionicLoading, ApiEndpoint, issueService, $ionicPush) {
     $scope.id = $stateParams.id;
     console.log('$scope.id: '+$scope.id);
    
     $scope.name = $stateParams.name;
     console.log('$scope.name: '+$scope.name);
    
     $scope.address = $stateParams.address;
     console.log('$scope.address: '+$scope.address);

     //IssueId
     $scope.fid = $localstorage.get('IssueId');
     console.log('$scope.fid: '+$scope.fid);

     //storename
     $scope.name = $stateParams.name;
     $localstorage.set('storeName',$stateParams.name);
     console.log('$scope.name: '+$scope.name);
     //StoreAddress
     $scope.address = $stateParams.address;
     $localstorage.set('storeAddress',$stateParams.address);
     console.log('$scope.address: '+$scope.address);
     $scope.arrCount = [0,1,2];
     $scope.Msg = "";
     $scope.status = $localstorage.get('status');

     this.setOtherTabs = function(){
        switch($localstorage.get('status')){
            case 'new' : $scope.twoTab ='inprogress'; $scope.threeTab ='resolved'; break;
            case 'inprogress' : $scope.twoTab = 'new'; $scope.threeTab = 'resolved'; break;
            case 'resolved': $scope.twoTab = 'new'; $scope.threeTab = 'inprogress';break;
            //default : alert('err');
        }
     };

     this.setOtherTabs();

     $scope.loadingIndicator = $ionicLoading.show({
        template: '<img ng-src="img/loading.gif" style="max-width:200px" />',
        animation: 'fade-in',
        showBackdrop: false,
        maxWidth: 200,
        showDelay: 500
    });

    $scope.issues = [];
    issueService.get({id: $scope.id, access_token: $scope.access_token}, function(data) {
        $scope.loadingIndicator.hide();
        $scope.issues = data.issues;
        $scope.arrCount = data.count;
        $scope.issueCount = $scope.arrCount[$scope.status];
        console.log('SUCCESS LISTING ISSUES!');
        console.log($scope.issues);       
    });

     /*
     $scope.test = $stateParams.test;
     if($scope.test == '1'){
        //$scope.new();
        reload();
     }
     */

    // DONE
    this.selectedTab=function(selectedTab, unTab1, unTab2){
        
        $scope.status = selectedTab;
        $localstorage.set('status', selectedTab);
        $scope.issueCount = $scope.arrCount[$scope.status];
        this.setOtherTabs;
        document.getElementById(selectedTab+"button").setAttribute("class", "button active");
        document.getElementById(unTab1+"button").setAttribute("class", "button");
        document.getElementById(unTab2+"button").setAttribute("class", "button");          
    };
    

    this.selectedTab($localstorage.get('status'), $scope.twoTab, $scope.threeTab);
    console.log($scope);
    $scope.access_token = $localstorage.get('access_token');
    console.log('access_token: '+$scope.access_token);       


    $scope.$on('$cordovaPush:notificationReceived', function(event, data) {
        issueService.get({id: $scope.id, access_token: $scope.access_token}, function(data) {  
            $scope.issues = data.issues;
            $scope.issueCount = data.count[$scope.status];
        });

  //         
    });
        //$scope.$apply();
})

.filter('split', function() {
        return function(input, splitChar) {
            // do some bounds checking here to ensure it has that index
            var splitChar = ';';
            var array = input.split(splitChar);

            if (array[5] != null) 
                return array[5];
            else if (array[4] != null) 
                return array[4];
            else if (array[3] != null) 
                return array[3];
        }
    });