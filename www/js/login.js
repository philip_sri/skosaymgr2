angular.module('skosayMgr.login', ['ionic','ionic.service.core', 'ionic.service.push'])
.controller('LoginController', function(myCache, $http, $state, $scope, $localstorage, ApiEndpoint, $ionicPush, $rootScope, $ionicUser, $ionicLoading){

    this.afterLogin = function (form, loginData){
      $http(form)
          .success(function(data, status, headers, config) {
            //myCache.put('access_token', data.token.access_token);
            //console.log("token: " +myCache.get('access_token'));
            $localstorage.set('access_token', data.token.access_token);
            console.log($localstorage.get('access_token'));

            $localstorage.set('email', data.email);
            $localstorage.set('pw', loginData.password);
            
            $localstorage.set('userId', data.userid);

            $localstorage.set('userName', data.name);
          




          var push = PushNotification.init({ "android": {"senderID": "778317139837"},
          "ios": {"alert": "true", "badge": "true", "sound": "true"}, "windows": {} }); 
          $rootScope.push = push; 
          push.on('registration', function(data){
              var token = {
                'token' : data.registrationId
              };
              console.log("Device token:" + token.token);
              var req2 = {
                      method: 'POST',
                      url: ApiEndpoint.url+'/v1/webhook',
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded' 
                      },           
                      data: "token=" + token.token  + '&email=' + $localstorage.get('email')   
              };

                    $http(req2)
                      .success(function(){
                          console.log('Succesfully signed up for push');
                      })
                      .error(function(data){
                          console.log('fail in changing deviceid')
                          /*for(var y in data){
                            alert(y);
                            for(var z in data[y]){
                              alert(z);
                              alert(data[y][z]);
                            }*/
                      });
          });
          // push.on('notification', function(data){
          //   alert(data.message);
          // })
          $rootScope.push.on('notification', function(data){
            // alert(data.message + "inside login");
          })

            $scope.id = data.userid;
            console.log('***SUCCESS***');
            console.log($scope);

            $state.go('tab.locations', { id: data.access_token });
          })
          .error(function(data, status, headers, config) {

            console.log('***ERROR***'+'\n\n'+
                  'Status: '+status+'\n\n'+
                  'Details: '+data.message+'\n\n');
            
            $localstorage.set('pw', '');

            $localstorage.set('email', '');    
            console.log(config,status, data, status,headers);
            //alert(data.message);
            $ionicLoading.hide();
            location.reload();
          });
    };

    
    this.loginData = {
        email: '',
        password: ''
    };



    

    this.login = function (loginData){
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = '';
//
      console.log(ApiEndpoint);


      var req = {
            method: 'POST',
            url: ApiEndpoint.url+'/v1/managerLogin',
//            url: ApiEndpoint.url+'/v1/oauth/access_token',
            headers: {
                 'Content-Type': 'application/x-www-form-urlencoded' 
            },           
            data: "client_id=" + loginData.email + "&client_secret=" + 12345 + "&username=" + loginData.email + "&password=" + loginData.password

/*            data: { 
                client_id: loginData.email,
                client_secret: "12345",
                username: loginData.email,
                password: loginData.password 
            }   */
      };
      console.log(req);
      this.afterLogin(req, loginData);
//    $http.post(ApiEndpoint.url+'/v1/managerLogin', $scope.loginData).
    };
     
      if($localstorage.get('pw') != undefined && $localstorage.get('email') != undefined){
        document.getElementById('submitButton').style.display = 'none';
        document.getElementById('falseButton').style.display = 'block';
       
        var logged = {
          method: 'POST',
          url: ApiEndpoint.url+'/v1/managerLogin',
  //            url: ApiEndpoint.url+'/v1/oauth/access_token',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded' 
          },           
          data: "client_id=" + $localstorage.get('email') + "&client_secret=" + 12345 + "&username=" + $localstorage.get('email') + "&password=" + $localstorage.get('pw')

        };

        var loginData = {
          email : $localstorage.get('email'),
          password : $localstorage.get('pw')
        };
        this.afterLogin(logged, loginData);
        //this.afterLogin(logged, loginData);
      //});
    }
})

.controller('BackController', function($scope,$ionicHistory,$interval)
{
    $scope.prevPage = function() {
      $ionicHistory.goBack();

      console.log('THIS'.$scope);
      $interval.cancel(promise);
      //document.getElementById('submitButton').style.display = '';
      //document.getElementById('falseButton').style.display = 'none';
    };
})
.controller("menuCtrl", function($scope, $ionicSideMenuDelegate, $state, ApiEndpoint, $http, $localstorage){
  $scope.openMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.signout = function() {
     var req = {
            method: 'GET',      
            url: ApiEndpoint.url+'/v1/logout', 
//            url: ApiEndpoint.url+'/v1/locations/',
/*            header: {
//                'Authorization': 'Bearer '+$scope.access_token */
            'Content-Type': 'application/x-www-form-urlencoded'
/*            data: "access_token=" + $localstorage.get('access_token')  */
    };
    $http(req).success(function(data){
      console.log(data.message);
      //$localstorage.removeItem('pw');
    });
    $localstorage.set('pw', '');
    $localstorage.set('email', '');
    $state.go('login',{},{reload: true});
    document.getElementById('submitButton').style.display = '';
    document.getElementById('submitButton').disabled = true;
    document.getElementById('falseButton').style.display = 'none';
    document.getElementById('email').value = "";
    document.getElementById('password').value = "";

  };

})













