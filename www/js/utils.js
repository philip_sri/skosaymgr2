angular.module('skosayMgr.utils', [])

.controller('NetCheckController', function($rootScope, $cordovaNetwork){
  //$rootScope.netCheck = false;
    
    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
      $rootScope.netCheck = false;  //won't show no net header 
      //alert('UP');
    });

    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
      $rootScope.netCheck = true;
      //alert('DOWN');
      //alert((String)$rootScope.net);
    });

})

.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);