// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('skosayMgr', ['ionic','ionic.service.core', 'ngResource',  'skosayMgr.services', 'skosayMgr.login', 'skosayMgr.messages', 'skosayMgr.utils', 'skosayMgr.issues',  'skosayMgr.locations','skosayMgr.db', 'ngCordova',  'ionic.service.push'])
.run(function($ionicPlatform, $cordovaSQLite, $ionicPush, $http, ApiEndpoint) {
  $ionicPlatform.ready(function(){

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
      
    db = $cordovaSQLite.openDB("my.db");
      
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS login (id integer primary key, client_id text, client_secret text, username text, password text, access_token text)");
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS stores (id integer primary key, address text, hasDept integer, photo text, status text, storeid text, storename text)");
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS issues (id integer primary key, userid text, storeid text, isDept integer, status text, time text)");
    $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS messages (id integer primary key, userid text, issueid text, message text, msgType text, photo text, msgStatus text, time text)");
      
  })
})




.constant('ApiEndpoint', {
// for: Actual
url: 'http://app.skosay.com/api'
// for: Brackets
//  url: 'http://127.0.0.1:52326/api'
    
// for: ionic serve
 // url: 'http://localhost:8100/api'
//url: 'http://192.168.5.125:8100/api'
  // url: 'http://192.168.5.124:8000/api'
  // url : 'http://192.168.1.12:8000/api'

})
.config(['$ionicAppProvider', function($ionicAppProvider) {
  // Identify app
  $ionicAppProvider.identify({
    // The App ID (from apps.ionic.io) for the server
    app_id: '61b45343',
    // The public API key all services will use for this app
    api_key: 'f8582524655152ebdea35c218ad9dbd1b6aaed41eb9bd359',
    // Set the app to use development pushes
    // dev_push: true,
    gcm_id : '778317139837'
  });
}])
.config(function($ionicConfigProvider){
  $ionicConfigProvider.tabs.position("bottom");
  $ionicConfigProvider.navBar.alignTitle("center");
})

.config(function($provide) {
    $provide.decorator('$state', function($delegate, $stateParams) {
        $delegate.forceReload = function() {
            return $delegate.go($delegate.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        };
        return $delegate;
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
   .state('login', {
    cache:false,
    url: "/login",
    templateUrl: "templates/login.html",
        controller: 'LoginController'
  })
      
  .state('tab', {
    url: "/tab",
    abstract: true,
    templateUrl: "templates/tabs.html"
  })

  // Each tab has its own nav history stack:

  .state('tab.locations', {
    cache: false,
    url: '/locations/:id',
    views: {
      'tab-dash': {
        templateUrl: 'templates/locations.html',
//        controller: 'LocationsController'
      }
    }
  })

  .state('tab.issues', {
    cache: false,
    url: '/issues/:id?name?address',
    views: {
      'tab-dash': {
        templateUrl: 'templates/issues.html',
        controller: 'NewIssuesController'
      }
    }
  })

.state('test', {
    cache: false,
    url: '/test/:id?name?address?status',
    views: {
      'tab-dash': {
        templateUrl: 'templates/issues.html',
        controller: 'NewIssuesController'
      }
    }
  })

  .state('messages', {
    cache: false,
    url: '/messages/:id?fid?name?address?DeptName',
    templateUrl: 'templates/messages.html',   
  })

  .state('tab.dash', {
    url: '/dash/:id',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
   //     controller: 'NewMessagesController'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
//          controller: 'InProcessController'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'ResolvedController'
      }
    }
  })
  ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

})

