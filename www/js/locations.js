angular.module('skosayMgr.locations', ['ionic','ionic.service.core', 'ionic.service.push'])

.controller('LocationsController', function(myCache, $http, $ionicPopup, $scope, $state, $stateParams, $ionicScrollDelegate, $localstorage, $ionicLoading, ApiEndpoint, $rootScope, $ionicPush) {
    $scope.id = $stateParams.id;
    
    console.log('Locations'+'\n');
    
    $scope.stores = [];
    //$scope.storeDepts = [];
    console.log($scope);

    //var cache = myCache.get('access_token');
    //console.log("token me: " +cache);
    
    $scope.access_token = $localstorage.get('access_token');
    console.log('access_token: '+$scope.access_token);

    $localstorage.set('status','new');
    
/* $http({method: 'GET', 
       url: '/entity/'+id+'?access_token='+token, 

       headers: {'Authorization': 'Bearer '+token}})
        .then(function(response){ // do stuff }); */

    var req = {
            method: 'GET',      
            url: ApiEndpoint.url+'/v1/locations/?access_token='+$scope.access_token, 
//            url: ApiEndpoint.url+'/v1/locations/',
/*            header: {
//                'Authorization': 'Bearer '+$scope.access_token */
            'Content-Type': 'application/x-www-form-urlencoded'
/*            data: "access_token=" + $localstorage.get('access_token')  */
    };
        
    console.log(req);
    
    $scope.loadingIndicator = $ionicLoading.show({
	    template: '<img ng-src="img/loading.gif" style="max-width:200px" />',
	    animation: 'fade-in',
	    showBackdrop: false,
	    maxWidth: 200,
	    showDelay: 500
	});
        
    $http(req)
        .success(function(data, status, headers, config) {
        
 //           console.log('Number of stores: '+data.stores.length);
//            console.log('Number of storesDepts: '+data.storeDept.length);
            
            //Hides the loading icon
            $scope.loadingIndicator.hide();
        
            $scope.stores=data.stores;
            //$scope.storeDepts=data.storeDept;
            console.log('***SUCCESS***');

            console.log($scope);

//            $state.go('tab.dash', { id: $scope.id });
        })
        .error(function(data, status, headers, config) {
            console.log(data, status, config);
            console.log('***ERROR***'+'\n\n'+
                  'Status: '+status+'\n\n'+
                  'Error: '+data.error+'\n\n'+
                  'Error Description: '+data.error_description+'\n\n'
                       );
           /* alert('***ERROR***'+'\n\n'+
                  'Status: '+status+'\n\n'+
                  'Error: '+data.error+'\n\n'+
                  'Error Description: '+data.error_description+'\n\n'
                 );
        */
        });

    // alert(window.location.href + "THIS IS LINK");
    $scope.push = $rootScope.push;
    // alert($scope.push);
    // $scope.push.on('notification', function(data){
    //   //use current location to check if event should run
    // alert(window.location.href + "THIS IS LINK");
    //   alert('locations' + data.message);
    // });
    $rootScope.push.on('notification', function(data){

            // alert(data.message + "inside somehwre");
            // alert(window.location.href);
    });
    // alert($scope.push);
    // alert('locations');

    // $scope.push = null;

})