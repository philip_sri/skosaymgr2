angular.module('skosayMgr.messages', ['ionic','ionic.service.core','ionic.service.push'])

.service('issuePasser', function() {

  // private variable
  var issueName = {};

  this.issueName = issueName;
})

.controller('uploadCtrl',function($scope, $rootScope, $cordovaCamera, $cordovaFileTransfer) {

    $rootScope.picData = undefined;

    $scope.takePicture = function() {
        var options = {
            quality: 70,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.CAMERA,
            encodingType: 0,
            targetHeight: 300,
            targetWidth: 300
        }
        navigator.camera.getPicture(onSuccess,onFail,options);
    };

    var onSuccess = function(FILE_URI) {
        console.log(FILE_URI);
        $rootScope.picData = FILE_URI;
        $scope.$apply();
        
        window.resolveLocalFileSystemURL(FILE_URI, function(entry){
               /*alert("entry.toURI: "+entry.toURI() + '\n\n' +
                    "FILE_URI: "+FILE_URI + '\n\n');*/
              //$scope.picData = entry.toURI();
           }, function(f){
              //alert('Some error occured: '+f.code);
           });
/*
        var options = {
            fileKey: "pictures",
            fileName: $scope.fid+"_msgpic.jpg",
            chunkedMode: false,
            mimeType: "image/png"
        };

        alert(options);

        $cordovaFileTransfer.upload("http://devapp.skosay.com/uploadAPI", $scope.picData, options).then(function(result) {
            console.log("SUCCESS: " + JSON.stringify(result.response));
            alert ("success" + JSON.stringify(result.response));
        }, function(err) {
            console.log("ERROR: " + JSON.stringify(err));
            alert ("error" + JSON.stringify(result.response));

        }, function (progress) {
            // constant progress updates
        });
*/
    };//onSuccess

    var onFail = function(e) {
        console.log("On Fail " + e);
    };
})

.controller('NewMessagesController', function(messageService, $http, $ionicPopup, $rootScope, $scope, $state, $stateParams, $ionicScrollDelegate, $localstorage, $interval, $ionicLoading, $ionicHistory, ApiEndpoint, issuePasser, $ionicPush) {
    $scope.push = $rootScope.push;
    $scope.push.on('notification', function(data){

        $scope.allMessages($stateParams);
    })

    $scope.loadingIndicator = $ionicLoading.show({
        template: '<img ng-src="img/loading.gif" style="max-width:200px" />',
        animation: 'fade-in',
        showBackdrop: false,
        maxWidth: 200,
        showDelay: 500
    });

    $i = 0;
   $scope.allMessages=function($stateParams){

        //StoreId
        $scope.id = $stateParams.id;
        $localstorage.set('storeId',$stateParams.id);
        console.log('$scope.id: '+$scope.id);
        //IssueId
        $scope.fid = $stateParams.fid;
        $localstorage.set('issueId',$stateParams.fid);
        console.log('$scope.fid: '+$scope.fid);
        //StoreName
        $scope.name = $stateParams.name;
        $localstorage.set('storeName',$stateParams.name);
        console.log('$scope.name: '+$scope.name);
        //StoreAddress
        $scope.address = $stateParams.address;
        $localstorage.set('storeAddress',$stateParams.address);
        console.log('$scope.address: '+$scope.address);
        //DeptName
        $scope.DeptName = $stateParams.DeptName;
        $localstorage.set('DeptName',$stateParams.DeptName);
        console.log('$scope.DeptName: '+$scope.DeptName);

       $scope.status = $localstorage.get('status');
            
        console.log('New Messages (Before GET): ');
        console.log($scope);
        
        $scope.access_token = $localstorage.get('access_token');
        console.log('access_token: '+$scope.access_token);
        
        $scope.userId = $localstorage.get('userId');
        console.log('userId: '+$scope.userId);
        
        $scope.userName = $localstorage.get('userName');
        console.log('userName: '+$scope.userName);
           
        $scope.loadingIndicator.hide();

        $scope.checkNew = function(){
            if($scope.issueStat == 'new'){
                return true;
            }
            else{
                return false;
            }
        };

        $scope.checkInProgress = function(){
            if($scope.issueStat == 'inprogress'){
                return true;
            }
            else{
                return false;
            }
        };   
        
        $scope.checkInResolved = function(){
            if($scope.issueStat == 'resolved')
                return true;
            else
                return false;
        }
        messageService.get({id: $scope.id, fid: $scope.fid, access_token: $scope.access_token}, function(data) {
            $scope.messages = data.messages;

            // Gets status of issue(table)
            $scope.issueStat = data.issuestat;            
            console.log('Status: '+$scope.issueStat);

             //AAA
            $localstorage.set('issueStat',$scope.issueStat);
            console.log('$scope.issueStatCheck: '+$scope.issueStat);
            //AAA

            console.log('SUCCESS LISTING MESSAGES!');
            $j = ($scope.messages).length;
            console.log('THISSSSSS --> '+$j+' & '+$i);
            if($i!=$j || $i==0){
            // Make the chat window scroll to the bottom 
                $ionicScrollDelegate.scrollBottom(true);
            }
        $interval.cancel(promise);
        })
    };//allMessages
    
    
    var promise = $interval(function(){
        $scope.allMessages($stateParams);
        $i = $j;
        console.log('HEYYYYY --> '+$j+' & '+$i);
    }, 2000);

    //NEW FUNCTION TO EDIT
    $scope.submitButtonStatus = function(submittedStatus) {
        console.log(submittedStatus);
            var req = {
                method: 'POST',
                url: ApiEndpoint.url+'/v1/changestatus/'+$scope.fid,

                headers: {
                     'Content-Type': 'application/x-www-form-urlencoded' 
                },           
                data: "statusChange=" + submittedStatus + "&access_token=" + $scope.access_token
            };

            $http(req)
            .success(function(data, status, headers, config) {
                console.log('Succeeded in changing message status.');
                //$state.go('tab.issues', {id: $scope.id, name: $scope.name, address: $scope.address}, {reload: true});
                $ionicHistory.goBack();
                })

            .error(function(data, status, headers, config) {
                console.log('ERRORERRORERRORERRORERRORERRORERROR');
            });
    };   
})

.controller('sendMessageController', function(messageService, $http, $ionicPopup, $rootScope, $scope, $state, $stateParams, $ionicScrollDelegate, $localstorage, ApiEndpoint, $cordovaFileTransfer) {
    
    $rootScope.message={
        
        reply: ''
        
    };
    
    //StoreId
    $scope.id = $localstorage.get('storeId');
    console.log('$scope.id: '+$scope.id);
    //IssueId
    $scope.fid = $stateParams.fid;
    console.log('$scope.fid: '+$scope.fid);
    //StoreName
    $scope.name = $localstorage.get('storeName');
    console.log('$scope.name: '+$scope.name);
    //StoreAddress
    $scope.address = $localstorage.get('storeAddress');
    console.log('$scope.address: '+$scope.address);
    //DeptName
    $scope.DeptName = $localstorage.get('DeptName');
    console.log('$scope.DeptName: '+$scope.DeptName);
        
    $scope.access_token = $localstorage.get('access_token');
    console.log('access_token: '+$scope.access_token);
    
    $scope.userId = $localstorage.get('userId');
    console.log('userId: '+$scope.userId);
    
    $scope.userName = $localstorage.get('userName');
    console.log('userName: '+$scope.userName);

    console.log('Before Send Messages: ');
    console.log($scope);    
    
    this.sendMessage=function(reply){
    //$scope.sendMessage = function(reply){
        console.log(reply);
        console.log(ApiEndpoint);



        if ($rootScope.picData != undefined) {
/*        if ($scope.picData != '' || $scope.picData != null || $scope.picData != undefined) { */
            var paramsObj = {
                reply: reply,
                issueid: $scope.fid,
                access_token: $scope.access_token
            };

            var options = {
            fileKey: "qwe",
            fileName: "msgPic.jpg",
            chunkedMode: false,
            mimeType: "image/png",
            params: paramsObj
            };

            //alert('picData: '+$scope.picData);

            $cordovaFileTransfer.upload(ApiEndpoint.url+'/v1/message/'+$scope.id+'/'+$scope.fid+'?reply='+reply+'&issueid='+$scope.fid+'&access_token='+$scope.access_token, $rootScope.picData, options).then(function(result) {
                console.log("SUCCESS: " + JSON.stringify(result.response));
               // alert ("success" + JSON.stringify(result.response));
                angular.element(document.getElementById('newMessageID')).scope().allMessages($stateParams);
                $rootScope.picData = undefined;
                   
                //alert($scope.access_token);
            }, function(err) {
                console.log("ERROR: " + JSON.stringify(err));
                //alert ("error");

            }, function (progress) {
                // constant progress updates
               // alert('sending');
            });
            
            /*var options = new FileUploadOptions();
                
            options.fileKey="qwe";
            options.fileName="msgPic.jpg";
            options.mimeType="image/jpeg";
            options.chunkedMode = false;


            var ft = new FileTransfer();

            ft.upload($rootScope.picData, encodeURI(ApiEndpoint.url+'/v1/message/'+$scope.id+'/'+$scope.fid+'?reply='+reply+'&issueid='+$scope.fid+'&access_token='+$scope.access_token), win, fail, options);

            function win(r) {
                console.log("Code = " + r.responseCode);
                console.log("Response = " + r.response);
                console.log("Sent = " + r.bytesSent);
            }

            function fail(error) {

                console.log("An error has occurred: Code = " + error.code);
                console.log("upload error source " + error.source);
                console.log("upload error target " + error.target);
            }*/
        } else {

            //alert(reply);
            var req = {
                method: 'POST',
                url: ApiEndpoint.url+'/v1/message/'+$scope.id+'/'+$scope.fid, 
                headers: {
                     'Content-Type': 'application/x-www-form-urlencoded' 
                },           
                data: "reply=" + reply + "&issueid=" + $scope.fid + "&access_token=" + $scope.access_token
            };
            
            console.log(req);
            
            $http(req).success(function(data, status, headers, config) {
                
                angular.element(document.getElementById('newMessageID')).scope().allMessages($stateParams);

                $scope.httpStatus = status;
                console.log('***SUCCESS***'+'\n\n'+
                            'Message: ' +data.message+'\n\n'+
                            'Status: '+status);
                console.log($scope);
                $rootScope.picData=undefined;
                $scope.$apply();
                //cleanup    
                $rootScope.message={

                    reply: ''

                };
                $rootScope.picData = undefined;

                // Make the chat window scroll to the bottom 
                $ionicScrollDelegate.scrollBottom(true);

                //$state.go('messages', {id: $scope.id, fid: $scope.fid, name: $scope.name, address: $scope.address, DeptName: $scope.DeptName}, {reload: true});

            })
            .error(function(data, status, headers, config) {
                console.log(data, status, config);
                console.log('***ERROR***'+'\n\n'+
                      'Status: '+status+'\n\n'+
                      'Error: '+data.error+'\n\n'+
                      'Error Description: '+data.error_description+'\n\n'
                           );
                /*alert('***ERROR***'+'\n\n'+
                      'Status: '+status+'\n\n'+
                      'Error: '+data.error+'\n\n'+
                      'Error Description: '+data.error_description+'\n\n'
                     );*/  
            });

        }
        //down code does not work. text area isn't cleared.
        //Clear Send Message Field
        $rootScope.message={
                reply: ''
        };
        document.getElementById('txtReply').value = "";
        console.log($scope);   
    };

})

.controller('changeStatusController', function(changeService, $ionicHistory, $http, $ionicPopup, $scope, $state, $stateParams, $ionicScrollDelegate, $localstorage, $ionicModal, ApiEndpoint, issuePasser) {

    $scope.id = $localstorage.get('storeId');
    console.log('$scope.id: '+$scope.id);
    //IssueId
    $scope.fid = $localstorage.get('issueId');
    console.log('$scope.fid: '+$scope.fid);
    //StoreName
    $scope.name = $localstorage.get('storeName');
    console.log('$scope.name: '+$scope.name);
    //StoreAddress
    $scope.address = $localstorage.get('storeAddress');
    console.log('$scope.address: '+$scope.address);
    //DeptName
    $scope.DeptName = $localstorage.get('DeptName');
    console.log('$scope.DeptName: '+$scope.DeptName);
        
    $scope.access_token = $localstorage.get('access_token');
    console.log('access_token: '+$scope.access_token);

    $scope.issueStat = $localstorage.get('issueStat');
    console.log('Issue Status: '+$scope.issueStat);

    $ionicModal.fromTemplateUrl('image-modal.html', { 
    scope: $scope
    }).then(function(modal) {
    $scope.modal = modal;
    });

    $scope.closeModal = function() {
      $scope.modal.hide();
    };
        
    $scope.changestatus = function() {
        $scope.modal.show();

        /*
        changeService.get({fid: $scope.fid, access_token: $scope.access_token}, function(data) {
            console.log('SUCCESS CHANGING STATUS');

            if(document.getElementById('txtReply').value = "" )
            {
                if($scope.status == "new"){
                    $state.go('tab.issues',{id: $scope.id, name: $scope.name, address: $scope.address});
                    }
                else if($scope.status == "inprogress"){
                    $state.go('tab.issues',{id: $scope.id, name: $scope.name, address: $scope.address});
                }
                else if($scope.status == "resolved"){
                    $state.go('tab.issues',{id: $scope.id, name: $scope.name, address: $scope.address});
                }
            }

            else
            {
                $scope.status = $localstorage.get('status');
                if($scope.status == "new"){
                    $state.go('tab.issues',{id: $scope.id, name: $scope.name, address: $scope.address});
                }
                else if($scope.status == "inprogress"){
                    $state.go('tab.issues',{id: $scope.id, name: $scope.name, address: $scope.address});
                }
                else if($scope.status == "resolved"){
                    $state.go('tab.issues',{id: $scope.id, name: $scope.name, address: $scope.address});
                }
            }//else
        })*/
    }//changeStatus
    
})



.filter('split', function() {
        return function(input, splitChar) {
            // do some bounds checking here to ensure it has that index
            var splitChar = ';';
            var array = input.split(splitChar);

            if (array[5] != null) 
                return array[5];
            else if (array[4] != null) 
                return array[4];
            else if (array[3] != null) 
                return array[3];
        }
    });