angular.module('skosayMgr.services', [])

.factory( 'myCache', function($cacheFactory) {
  var myCache = $cacheFactory('myCache');
  return myCache;
})

.factory('issueService', function($resource, $http, ApiEndpoint){
  return $resource(ApiEndpoint.url+'/v1/issuelist/:id/?access_token=:access_token');
})

//TWEAK
.factory('changeService', function($resource, $http, ApiEndpoint){
  return $resource( ApiEndpoint.url+'/v1/changestatus/:fid/?access_token=:access_token');
})

.factory('messageService', function($resource, $state, $q, $http, $localstorage, ApiEndpoint){
  return $resource(ApiEndpoint.url+'/v1/message/:id/:fid/?access_token=:access_token');
})

//

/*.factory('cameraService', function($q) {

  return {
    getPicture: function(options) {
      var q = $q.defer();

      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);

      return q.promise;
    }
  }
});*/